uses dos, sysUtils, crt;

var
  h, m, s, d : word;
  hl, l,
  locOffset, hLessionsStart, mLessionsStart, lession, pause, changePause, bigPause, upK : integer;
  order, cfgPath : string;
  exit : boolean;



function SjIoFileTextCfgGVP (pathToFile : string; parameterName : string): string;
  var
    f : text;
    ch : char;
    parameterFound, comment, EOString, valueCharsSwitcher, admissibleCharacter,
      EOFError : boolean;
    valueChars, parameterChars : string;
begin
  if FileExists (pathToFile)
  then
    begin
      parameterFound := false;
      assign(f, pathToFile);
      reset (f);
      repeat
           comment := false;
           EOString := false;
           valueCharsSwitcher := false;
           parameterChars := '';
           valueChars := '';
           repeat
                read (f,ch);
                EOFError := EOF(f);
                case ch of
                  '{' : comment := true;
                  '}' : comment := false;
                end;
                if not comment
                then begin
                      case ch of
                        '=' : valueCharsSwitcher := true;
                        ';' : EOString := true;
                      end;
                      if ch in ['-'..':']
                      then admissibleCharacter := true
                      else if ch in ['A'..'_']
                           then admissibleCharacter := true
                           else if ch in ['a'..'z']
                                then admissibleCharacter := true
                                else admissibleCharacter := false;
                      if admissibleCharacter
                      then if valueCharsSwitcher
                           then valueChars := valueChars + ch
                           else parameterChars := parameterChars + ch
                    end;
              until  EOFError or  EOString;
           parameterFound := parameterChars = parameterName;
           if parameterFound
           then SjIoFileTextCfgGVP := valueChars;
           if not parameterFound and EOFError
           then SjIoFileTextCfgGVP := '0';
      until  EOFError or parameterFound;
    end
  else SjIoFileTextCfgGVP := '1';
end;

function cfg (pathToCfg : string; param : string) : string;
  var
    s : string;
  begin
    cfg := SjIoFileTextCfgGVP(pathToCfg, param);
  end;

procedure init;
  var
    f : text;
  begin
    cfgPath := 'sjltimer.cfg';
    if not FileExists (cfgPath)
    then
      begin
        assign (f, cfgPath);
        rewrite (f);
        writeln (f,'location_offset = 10;');
        writeln (f,'hour_lessions_start = 8;');
        writeln (f,'minute_lessions_start = 0;');
        writeln (f,'Lession = 45;');
        writeln (f,'Pause = 5;');
        writeln (f,'Change_pause = 10;');
        writeln (f,'Big_pause = 40;');
        writeln (f,'order = LPLCLPLBLPLCLPL;');
        writeln (f,'upK = 1000;');
        close (f);
      end;
    locOffset := StrToInt(cfg(cfgPath, 'location_offset'));
    hLessionsStart := StrToInt(cfg(cfgPath, 'hour_lessions_start'));
    mLessionsStart := StrToInt(cfg(cfgPath, 'minute_lessions_start'));
    lession := StrToInt(cfg(cfgPath, 'Lession'));
    pause := StrToInt(cfg(cfgPath, 'Pause'));
    changePause := StrToInt(cfg(cfgPath, 'Change_pause'));
    bigPause := StrToInt(cfg(cfgPath, 'Big_pause'));
    order := cfg(cfgPath, 'order');
    upK := StrToInt(cfg(cfgPath, 'upK'));
  end;

procedure timeUpdate;

  begin
    getTime(h,m,s,d);
    hl := h + locOffset;
    if hl > 23
    then hl := hl - 24;
    if hl < 0
    then hl := hl + 24;
  end;

procedure testinit;
  begin
    timeUpdate;
    writeln ();
    writeln ('<< begin testinit');
    writeln ('Time UTC ',h,':',m,':',s,':',d,';');
    writeln ('Time Loc ',hl,':',m,':',s,':',d,';');
    writeln ('locOffset = ',locOffset,';');
    writeln ('hLessionsStart = ',hLessionsStart,';');
    writeln ('mLessionsStart = ',mLessionsStart,';');
    writeln ('lession = ',lession,';');
    writeln ('pause = ',pause,';');
    writeln ('changePause = ',changePause,';');
    writeln ('bigPause = ',bigPause,';');
    writeln ('order = "',order,'";');
    writeln ('<< end testinit');
    writeln ();
  
  end;
  
  function MtoHM (min : integer) : string;
  var hour : integer;
  begin
    hour := 0;// hLessionsStart;
  //  min := 0;//min + mLessionsStart;
    while min > 59 do
      begin
        min := min - 60;
        hour := hour + 1;
      end;
    if min > 9
    then MtoHM := IntToStr(hour)+':'+IntToStr(min)
    else MtoHM := IntToStr(hour)+':0'+IntToStr(min)
  end;
  
  function HMtoM : integer;
  begin
    HMtoM := (hl*60)+m;
  end;

procedure mainGUI(SX : integer; SY : integer);
var
  i, NY, NL, min : integer;
  ch : char;
  begin
    NY := SY;
    NL := 1;
    min := ((hLessionsStart*60)+mLessionsStart);
    for i:=1 to length(order) do
    begin
      ch := order[i];
      case ch of
      'L': begin
             gotoxy(SX+2, NY);
             
             if (hl*60)+m > min-1
             then
               begin
                 gotoxy(SX, NY);
                 textcolor(4);
                 write ('-');
                 if ((hl*60)+m < min+lession) and (0 <= (min+lession)-((hl*60)+m+1)) //+((hLessionsStart*60)+mLessionsStart)
                 then
                   begin
                     
                     gotoxy(SX+20, NY);
                     textcolor(2);
                     write ((min+lession{+((hLessionsStart*60)+mLessionsStart)})-((hl*60)+m+1), ':', 59-s,#32);
                     gotoxy(SX, NY);
                     textcolor(7);
                     write('#');
                   end;
               end;
             textcolor (2);
             write (NL,'. ',MtoHM(min),'-',MtoHM(min+lession),#32,#32);
             NL := NL + 1;
             NY := NY + 1;
             min := min + lession;
           end;
      'P': begin
             gotoxy(SX+3, NY);
             textcolor (3);
         //    write ('Pause ',pause,'min');
             min := min + pause;
         //    NY := NY + 1;
             
           end;
       'C': begin
              gotoxy(SX+2, NY);
              textcolor(4);
              write ('Long Pause ',changePause,'min');
              NY := NY + 1;
              min := min + changePause;
            end;
       'B': begin
              gotoxy(SX+2, NY);
              textcolor(5);
              write ('Big pause ',bigPause,'min');
              NY := NY + 1;
              min := min + bigPause;
            end;
      end;
      if hl*60+m > min 
      then exit := true
      else exit := false
    end; 
  end;



begin
  init;
  repeat;
    timeUpdate;
//  testinit;
    mainGUI(1,1);
    writeln;
    writeln ('Time Loc ',hl,':',m,':',s,#32);
    writeln ('Time UTC ',h,':',m,':',s,#32);
    writeln ('v0.1.5.1 by SeaJay Greeny');
    delay (upk);
    clrscr;
  until exit;
end.
